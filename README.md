H5P Survey
==========

H5P Mark the Letters allows the user to find and mark specific letters in a
paragraph as mentioned in the task description.

## Screenshots

<img src="https://gitlab.com/icfoss/h5p-marktheletters/raw/master/screenshots/marktl.png"/>


## License

[GPL v3](LICENSE)


## Credits

This content type is developed under the Media & Learning Lab of [ICFOSS](https://icfoss.in)

